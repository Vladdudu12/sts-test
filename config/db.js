const Sequelize = require("sequelize");

const sequelize = new Sequelize("sts_test", "root", "", {
    dialect: "mysql",
    host: "localhost",
    define: {
        timestamp: true,
    },
});

module.exports = sequelize;