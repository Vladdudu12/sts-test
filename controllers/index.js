const trupa = require('./trupa');
const user = require('./user');

const controllers = {
    trupa, 
    user
};

module.exports = controllers;