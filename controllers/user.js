const UserDb = require('../models').User;

const controller = {
    getAllUsers: async (req, res) => {
        UserDb.findAll().then((users) => {
            res.status(200).send(users);
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    getUserById: async (req,res) => {
        const { id } = req.params;
        if(!id) {
            res.status(400).send({message: "ID not provided!"});
        }

        UserDb.findByPk(id).then((user) => {
            res.status(200).send({ user });
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    addUser: async (req, res) => {
        const { username, password, role } = req.body;
        UserDb.create({username, password, role}).then((user) => {
            res.status(201).send(user);
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    deleteUserById: async (req, res) => {
        const { id } = req.params;
        UserDb.findByPk(id).then((user) => {
            if(user){
                user.destroy().then(()=>{
                    res.status(200).send({message: "User sters!"});
                }).catch((err) => {
                    console.log(err);
                    res.status(500).send({message: "Server Error!"});
                });
            }
            else{
                res.status(404).send({message: "ID User inexistent!"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    updateUserById: async (req, res) => {
        const { id } = req.params;
        const { username, password, role } = req.body;
        UserDb.findByPk(id).then((user) => {
            if(user){
                user.update({username: username, password: password, role: role}).then(() => {
                    res.status(200).send({message: "User modificat!"});
                }).catch((err) => {
                    console.log(err);
                    res.status(500).send({message: "Server Error!"});
                })
            }
            else{
                res.status(404).send({message: "ID User inexistent!"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },
};

module.exports = controller;