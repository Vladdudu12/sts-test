const TrupaDb = require("../models").Trupa;

const controller = {
    getAllTrupe: async(req, res) => {
        TrupaDb.findAll().then((trupe) => {
            res.status(200).send(trupe);
        }).catch((err) =>{
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    getTrupaById: async(req, res) => {
        const { id } = req.params;
        if(!id) {
            res.status(400).send({message: "ID not provided!"});
        }

        TrupaDb.findByPk(id).then((trupa) => {
            res.status(200).send({ trupa });
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    addTrupa: async(req,res) => {
        const {        
            numeTrupa,numePiesa,autor,durata,timpMontare,cerinteDecor,linkScenariu,localitate,gen,regia,descriere,
            timpDemontare,linkFilmare,observatii,categorie,cazare,aerLiber,detaliiAerLiber,numarMembri,
            numeReprezentant,prenumeReprezentant,numarTelefon,email,nivelStudii,undeAuzit,newsletter
        } = req.body;

        TrupaDb.create({            
            numeTrupa,numePiesa,autor,durata,timpMontare,cerinteDecor,linkScenariu,localitate,gen,regia,descriere,
            timpDemontare,linkFilmare,observatii,categorie,cazare,aerLiber,detaliiAerLiber,numarMembri,
            numeReprezentant,prenumeReprezentant,numarTelefon,email,nivelStudii,undeAuzit,newsletter}).then((trupa) => {
                res.status(201).send(trupa);
            }).catch((err) => {
                console.log(err);
                res.status(500).send({message: "Server Error!"});
            });
    },

    deleteTrupaById: async (req,res) =>{
        const { id } = req.params;
        TrupaDb.findByPk(id).then((trupa) => {
            if(trupa){
                trupa.destroy().then(()=> {
                    res.status(200)>send({message: "Trupa Stearsa!"});
                });
            }
            else {
                res.status(404).send({message: "ID Trupa inexistent!"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    updateTrupaById: async (req,res) => {
        const { id } = req.params;
        const {        
            numeTrupa,numePiesa,autor,durata,timpMontare,cerinteDecor,linkScenariu,localitate,gen,regia,descriere,
            timpDemontare,linkFilmare,observatii,categorie,cazare,aerLiber,detaliiAerLiber,numarMembri,
            numeReprezentant,prenumeReprezentant,numarTelefon,email,nivelStudii,undeAuzit,newsletter
        } = req.body;

        TrupaDb.findByPk(id).then((trupa) => {
            if(trupa){
                trupa.update({
                    numeTrupa:numeTrupa,numePiesa:numePiesa,autor:autor,durata:durata,timpMontare:timpMontare,cerinteDecor:cerinteDecor,
                    linkScenariu:linkScenariu,localitate:localitate,gen:gen,regia:regia,descriere:descriere,timpDemontare:timpDemontare,
                    linkFilmare:linkFilmare,observatii:observatii,categorie:categorie,cazare:cazare,aerLiber:aerLiber,detaliiAerLiber:detaliiAerLiber,
                    numarMembri:numarMembri,numeReprezentant:numeReprezentant,prenumeReprezentant:prenumeReprezentant,numarTelefon:numarTelefon,
                    email:email,nivelStudii:nivelStudii,undeAuzit:undeAuzit,newsletter:newsletter}).then(() => {
                        res.status(200).send({message: "Trupa modificata!"});
                    }).catch((err) => {
                        console.log(err);
                        res.status(500).send({message: "Server Error!"});
                    });
            }
            else{
                res.status(404).send({message: "ID Trupa inexistent!"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },
};

module.exports = controller;