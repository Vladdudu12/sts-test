const Sequelize = require("sequelize");
const db = require("../config/db");
const TrupaModel = require("./trupa");
const UserModel = require("./user");

const Trupa = TrupaModel(db, Sequelize);
const User = UserModel(db, Sequelize);

module.exports = {
    Trupa,
    User,
    connection: db,
};