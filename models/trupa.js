module.exports = (sequelize, DataTypes) => {
    return sequelize.define("trupa", {
        numeTrupa: DataTypes.STRING,
        numePiesa: DataTypes.STRING,
        autor: DataTypes.STRING,
        durata: DataTypes.INTEGER,
        timpMontare: DataTypes.INTEGER,
        cerinteDecor: DataTypes.STRING,
        linkScenariu: DataTypes.STRING,
        localitate: DataTypes.STRING,
        gen: DataTypes.STRING,
        regia: DataTypes.STRING,
        descriere: DataTypes.STRING,
        timpDemontare: DataTypes.INTEGER,
        linkFilmare: DataTypes.STRING,
        observatii: DataTypes.STRING,
        categorie: DataTypes.STRING,
        cazare: DataTypes.BOOLEAN,
        aerLiber: DataTypes.BOOLEAN,
        detaliiAerLiber: DataTypes.STRING,
        numarMembri: DataTypes.INTEGER,
        numeReprezentant: DataTypes.STRING,
        prenumeReprezentant: DataTypes.STRING,
        numarTelefon: DataTypes.STRING,
        email: DataTypes.STRING,
        nivelStudii: DataTypes.STRING,
        undeAuzit: DataTypes.STRING,
        newsletter: DataTypes.BOOLEAN
    });
};