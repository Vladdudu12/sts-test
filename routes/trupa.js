const express = require('express');
const router = express.Router();
const trupaController = require('../controllers').trupa;

router.get('/', trupaController.getAllTrupe);
router.get('/:id', trupaController.getTrupaById);
router.post('/', trupaController.addTrupa);
router.put('/:id',trupaController.updateTrupaById);
router.delete('/:id', trupaController.deleteTrupaById);

module.exports = router;