const express = require('express');
const router = express.Router();
const trupaRouter = require('./trupa');
const userRouter = require('./user');

router.use('/trupe',trupaRouter);
router.use('/users',userRouter);

module.exports = router;